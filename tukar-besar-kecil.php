<?php
/*function tukar_besar_kecil($string){
    $alphas = range('a','z');
    $exchangedAlphas = "";
            foreach(str_split($string) as $str){
                    if(ctype_lower($str)){
                        $exchangedAlphas .= strtoupper($str);
                }elseif(ctype_upper($str)){
                    $exchangedAlphas .= strtolower($str);
                }
        }
    return $exchangedAlphas ."<br>";//kode di sini
}*/

function tukar_besar_kecil($string){
    $alphas = "abcdefghijklmnopqrstuvwxyz";
    $exchangedAlphas = "";
            for($a = 0; $a < strlen($string); $a++){
                $huruf_kecil = strpos($alphas, $string[$a]);
                    if($huruf_kecil == null){
                        $exchangedAlphas .= strtolower($string[$a]);
                }else{
                    $exchangedAlphas .= strtoupper($string[$a]);
                }
        }
    return $exchangedAlphas ."<br>";//kode di sini
}
// TEST CASES
echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

?>