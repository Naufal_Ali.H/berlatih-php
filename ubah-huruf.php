<?php
function ubah_huruf($string){
    $alphas = range('a', 'z');
    $changedAlphas = "";
            foreach(str_split($string) as $str){
                for($i = 0; $i < count($alphas); $i++){
                    if(strtolower($str) == $alphas[$i]){
                        $changedAlphas .= $alphas[$i+1];
                }
            }
        }
    return $changedAlphas ."<br>";
        //kode di sini
};

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu

?>